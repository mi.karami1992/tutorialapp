package ir.mk.tutorial.mvp.view

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.FrameLayout
import ir.mk.tutorial.R
import ir.mk.tutorial.activity.MainActivity
import ir.mk.tutorial.databinding.SettingsBinding

class ViewSetting(contextInstance: Context) : FrameLayout(contextInstance) {
    var binding = SettingsBinding.inflate(LayoutInflater.from(context))
    var sharedPreferences: SharedPreferences=context.getSharedPreferences("pref", Context.MODE_PRIVATE)
    fun configLangCheckBox(arguments: Bundle?) {
        val lang = arguments?.getString("language", "en")
        if (lang == "fa") {
            binding.radioPersian.isChecked = true
        } else {
            binding.radioEnglish.isChecked = true
        }
    }

    fun configDarkMode(arguments: Bundle?) {
        val nightMode = arguments?.getBoolean("darkMode", false)
        binding.darkSwitch.isChecked = nightMode!!
    }

    fun initListener() {
        binding.darkSwitch.setOnCheckedChangeListener { _, b ->
//            (this.context as? MajorActivity)?.setNightMode(b)
            saveDarkMode(b)
            (this.context as? MainActivity)?.recreate()
        }
        binding.radioLang.setOnCheckedChangeListener { _, radioBtnId ->
            var lang = "en"
            when (radioBtnId) {
                R.id.radioPersian -> {
                    lang = "fa"
                }

                R.id.radioEnglish -> {
                    lang = "en"
                }
            }
            saveLanguage(lang)
            (this.context as? MainActivity)?.recreate()
        }
    }

    fun saveDarkMode(isNightModeEnabled: Boolean) {
        val editor = sharedPreferences.edit()
        editor.putBoolean("nightMode", isNightModeEnabled)
        editor.apply()
    }

    fun saveLanguage(language: String) {
        val editor = sharedPreferences.edit()
        editor.putString("language", language)
        editor.apply()
    }


}
