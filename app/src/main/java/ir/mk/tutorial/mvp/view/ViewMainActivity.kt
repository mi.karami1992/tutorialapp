package ir.mk.tutorial.mvp.view

import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatDelegate
import ir.mk.tutorial.fragments.LessonsFragment
import ir.mk.tutorial.fragments.LoginFragment
import ir.mk.tutorial.util.MyContextWrapper
import ir.mk.tutorial.R
import ir.mk.tutorial.fragments.SettingsFragment
import ir.mk.tutorial.activity.MainActivity
import ir.mk.tutorial.databinding.ActivityMainBinding

class ViewMainActivity(contextInstance: Context) : FrameLayout(contextInstance) {
    val binding = ActivityMainBinding.inflate(LayoutInflater.from(context))
    private val activity = (context as? MainActivity)!!
    fun setNavigation(darkMode:Boolean,language:String) {
        activity.setSupportActionBar(binding.content.toolbar)
        val toggle = ActionBarDrawerToggle(
            activity,
            binding.drawerlayout,
            binding.content.toolbar,
            R.string.open,
            R.string.close
        )
        toggle.isDrawerIndicatorEnabled = true
        binding.drawerlayout.addDrawerListener(toggle)
        toggle.syncState()
        activity.supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container_view, LessonsFragment(),"lessons").commit()
        binding.navView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.lesson_list -> {
                    activity.supportFragmentManager.beginTransaction()
                        .replace(R.id.fragment_container_view, LessonsFragment(),"lesson").commit()
                    binding.drawerlayout.closeDrawers()
                    true
                }

                R.id.setting_menu -> {
                    val bundle = Bundle()
                    bundle.putBoolean("darkMode", darkMode)
                    bundle.putString("language", language)
                    val settingFragment = SettingsFragment();
                    settingFragment.arguments = bundle
                    activity.supportFragmentManager.beginTransaction()
                        .replace(R.id.fragment_container_view, settingFragment,"setting").commit()
                    binding.drawerlayout.closeDrawers()
                    true
                }

                R.id.login_menu -> {
                    activity.supportFragmentManager.beginTransaction()
                        .replace(R.id.fragment_container_view, LoginFragment(),"loggin").commit()

                    binding.drawerlayout.closeDrawers()
                    true
                }

                else -> {
                    false
                }
            }
        }
    }



     fun setDarkMode(nightMode: Boolean) {
        when (this.resources?.configuration?.uiMode?.and(Configuration.UI_MODE_NIGHT_MASK)) {
            Configuration.UI_MODE_NIGHT_YES -> {
                if (!nightMode)
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            }

            Configuration.UI_MODE_NIGHT_NO -> {
                if (nightMode)
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            }

            Configuration.UI_MODE_NIGHT_UNDEFINED -> {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            }
        }

    }


    fun setLanguage(lang: String) {
        MyContextWrapper.wrap(context, lang)
        resources.updateConfiguration(resources.configuration, resources.displayMetrics)
    }





}
