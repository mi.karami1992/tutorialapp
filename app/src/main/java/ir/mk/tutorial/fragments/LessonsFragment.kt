package ir.mk.tutorial.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import ir.mk.tutorial.R
import ir.mk.tutorial.mvp.model.ModelLessons
import ir.mk.tutorial.mvp.presenter.PresenterLessons
import ir.mk.tutorial.mvp.view.ViewLessonsFragment

class LessonsFragment : Fragment(R.layout.lesson_list) {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = ViewLessonsFragment((this.context)!!)
        val model = ModelLessons()
        val presenter = PresenterLessons(view, model)
        presenter.onCreate()
        return view.binding.root
    }
}