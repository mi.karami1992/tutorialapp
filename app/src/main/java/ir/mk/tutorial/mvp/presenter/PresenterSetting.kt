package ir.mk.tutorial.mvp.presenter

import android.os.Bundle
import ir.mk.tutorial.mvp.view.ViewSetting

class PresenterSetting(
    private val view: ViewSetting
) {
    fun onCreate(arguments: Bundle?) {
        view.configLangCheckBox(arguments)
        view.configDarkMode(arguments)
        view.initListener()
    }

}