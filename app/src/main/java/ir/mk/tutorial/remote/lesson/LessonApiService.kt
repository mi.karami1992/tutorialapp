package ir.mk.tutorial.remote.lesson

import ir.mk.tutorial.dto.LessonResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface LessonApiService {
    @GET("chatgpt")
    fun getLessons( ): Call<List<LessonRequestDto>>

    @GET("chatgpt/{id}")
    fun getLesson(@Path("id") id: Int):Call<LessonResponse>
}