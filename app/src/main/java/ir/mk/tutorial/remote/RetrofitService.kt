package ir.mk.tutorial.remote.lesson

import ir.mk.tutorial.remote.lesson.LessonApiService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitService {
    private const val url = "http://10.0.2.2:8080/lesson/"
    private val retrofit= Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create())
        .build()
    val apiService: LessonApiService = retrofit.create(LessonApiService::class.java)
}