package ir.mk.tutorial.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import ir.mk.tutorial.R
import ir.mk.tutorial.mvp.presenter.PresenterSetting
import ir.mk.tutorial.mvp.view.ViewSetting

class SettingsFragment : Fragment(R.layout.settings) {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = ViewSetting((this.context)!!);
        val presenter  = PresenterSetting(view)
        presenter.onCreate(arguments)
        return view.binding.root
    }
}