package ir.mk.tutorial.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ir.mk.tutorial.mvp.presenter.PresenterShowPhotoActivity
import ir.mk.tutorial.mvp.view.ViewShowPhotoActivity

class ShowPhotoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val view  =  ViewShowPhotoActivity(this)
        val presenter = PresenterShowPhotoActivity(view)
        presenter.onCreate()
        setContentView(view.binding.root)


    }
}