package ir.mk.tutorial.remote.lesson

import ir.mk.tutorial.dto.LessonResponse


interface LessonRequest {
    fun onSuccess(data:LessonResponse)
    fun onNotSuccess(message:String)
    fun onError(message: String)
}