package ir.mk.tutorial.mvp.view

import android.content.Context
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ir.mk.tutorial.dto.Lesson
import ir.mk.tutorial.adapter.LessonAdapter
import ir.mk.tutorial.databinding.LessonListBinding

class ViewLessonsFragment(contextInstance: Context) : FrameLayout(contextInstance) {
      var binding = LessonListBinding.inflate(LayoutInflater.from(context))
      fun showAdapter(lessons: ArrayList<Lesson>) {
            val adapter = LessonAdapter(lessons)
            binding.lessonsRecyclerView.layoutManager = LinearLayoutManager(
                  this.context, RecyclerView.VERTICAL, false
            )
            binding.lessonsRecyclerView.adapter = adapter
      }
}