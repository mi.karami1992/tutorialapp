package ir.mk.tutorial.mvp.view

import android.content.Context
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ir.mk.tutorial.adapter.SegmentAdapter
import ir.mk.tutorial.databinding.FragmentLessonSegmentsBinding
import ir.mk.tutorial.dto.Segment

class ViewLesson(contextInstance: Context) : FrameLayout(contextInstance) {
      var binding = FragmentLessonSegmentsBinding.inflate(LayoutInflater.from(context))
      fun showSegments(data: List<Segment>) {
            val adapter = SegmentAdapter(data)
            binding.segmentRecyclerView.layoutManager = LinearLayoutManager(
                  this.context, RecyclerView.VERTICAL, false
            )
            binding.segmentRecyclerView.adapter = adapter
      }
      fun showTitle(title:String){
            binding.txtLessonTitle.text = title
      }
}