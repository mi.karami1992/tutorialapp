package ir.mk.tutorial.mvp.model

import ir.mk.tutorial.dto.LessonResponse
import ir.mk.tutorial.mvp.OnLessonBindData
import ir.mk.tutorial.remote.lesson.LessonApiRepository
import ir.mk.tutorial.remote.lesson.LessonRequest

class ModelLesson {
    fun fetchLesson(onLessonBindData: OnLessonBindData,lessonId:Int) {
        LessonApiRepository.instance.getLesson(object : LessonRequest {
            override fun onSuccess(data: LessonResponse) {
                onLessonBindData.loadLesson(data)
            }

            override fun onNotSuccess(message: String) {
            }

            override fun onError(message: String) {
            }


        },lessonId )


    }

}