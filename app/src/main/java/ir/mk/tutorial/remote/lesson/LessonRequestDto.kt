package ir.mk.tutorial.remote.lesson

import com.google.gson.annotations.SerializedName

data class LessonRequestDto(
    @SerializedName("id") val id: Long,
    @SerializedName("title")val title: String
)