package ir.mk.tutorial.mvp.presenter

import ir.mk.tutorial.mvp.view.ViewShowPhotoActivity

class PresenterShowPhotoActivity(
    private val view:ViewShowPhotoActivity
) {
    fun onCreate() {
        view.initListener()
        view.loadImage()
    }


}