package ir.mk.tutorial.mvp.model

import ir.mk.tutorial.dto.Lesson
import ir.mk.tutorial.mvp.OnLessonBindData
import ir.mk.tutorial.remote.lesson.LessonApiRepository
import ir.mk.tutorial.remote.lesson.LessonListRequest
import ir.mk.tutorial.remote.lesson.LessonRequestDto

class ModelLessons {
    fun fetchAllLessons(onLessonBindData: OnLessonBindData) {
        LessonApiRepository.instance.getLessons(object : LessonListRequest {

            override fun onSuccess(data: List<LessonRequestDto>) {
                val lessons = ArrayList<Lesson>()
                for (lessonRequstDto in data) {
                    lessons.add(Lesson(lessonRequstDto.id.toInt(), lessonRequstDto.title))
                }
                onLessonBindData.loadLessons(lessons)
            }

            override fun onNotSuccess(message: String) {
            }

            override fun onError(message: String) {
            }
        })

    }

}