package ir.mk.tutorial.dto

data class Lesson( val id:Int, val title:String)
