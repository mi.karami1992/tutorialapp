package ir.mk.tutorial.dto

data class Segment(
    val id: Int,
    val paragraph: String,
    val imageUrl: String
)