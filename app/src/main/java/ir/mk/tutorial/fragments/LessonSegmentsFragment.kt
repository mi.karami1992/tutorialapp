package ir.mk.tutorial.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ir.mk.tutorial.R
import ir.mk.tutorial.databinding.FragmentLessonSegmentsBinding
import ir.mk.tutorial.mvp.model.ModelLesson
import ir.mk.tutorial.mvp.presenter.PresenterLesson
import ir.mk.tutorial.mvp.view.ViewLesson


class LessonSegmentsFragment : Fragment(R.layout.fragment_lesson_segments) {

    private var _binding: FragmentLessonSegmentsBinding? = null // Declare a private nullable variable for binding
    private val binding get() = _binding!! // Create a non-null reference to the binding
    private var lessonIdParameter: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        lessonIdParameter = arguments?.getString("lessonId")
        val view = ViewLesson((this.context)!!)
        val model = ModelLesson()
        val presenter = PresenterLesson(view, model)
        presenter.onCreate(lessonIdParameter!!.toInt())
        return view.binding.root
    }


}