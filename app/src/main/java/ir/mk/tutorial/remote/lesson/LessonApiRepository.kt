package ir.mk.tutorial.remote.lesson

import ir.mk.tutorial.dto.LessonResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LessonApiRepository {
    companion object {
        private var apiRepository: LessonApiRepository? = null
        val instance: LessonApiRepository
            get() {
                if (apiRepository == null) apiRepository = LessonApiRepository()
                return apiRepository!!
            }
    }

    fun getLessons(request: LessonListRequest) {
        RetrofitService.apiService.getLessons().enqueue(
            object : Callback<List<LessonRequestDto>> {
                override fun onResponse(call: Call<List<LessonRequestDto>>, response: Response<List<LessonRequestDto>>) {
                    if (response.isSuccessful)
                        request.onSuccess(response.body() as List<LessonRequestDto>)
                    else request.onNotSuccess(message = "خطایی رخ داده")
                }

                override fun onFailure(call: Call<List<LessonRequestDto>>, t: Throwable) {
                    request.onNotSuccess(t.message.toString())
                }

            }
        )
    }

    fun getLesson(request: LessonRequest, id:Int) {
        RetrofitService.apiService.getLesson(id).enqueue(
            object : Callback<LessonResponse> {
                override fun onResponse(
                    call: Call<LessonResponse>,
                    response: Response<LessonResponse>
                ) {
                    if (response.isSuccessful)
                        request.onSuccess(response.body() as LessonResponse)
                    else request.onNotSuccess(message = "خطایی رخ داده")
                }

                override fun onFailure(call: Call<LessonResponse>, t: Throwable) {
                    request.onNotSuccess(t.message.toString())
                }


            }
        )
    }



}
