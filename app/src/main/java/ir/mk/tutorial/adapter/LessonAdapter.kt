package ir.mk.tutorial.adapter

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import ir.mk.tutorial.dto.Lesson
import ir.mk.tutorial.fragments.LessonSegmentsFragment
import ir.mk.tutorial.R
import ir.mk.tutorial.databinding.LessonItemBinding

class LessonAdapter(
    private val lessons: ArrayList<Lesson>
) : RecyclerView.Adapter<LessonAdapter.LessonViewHolder>() {
    inner class LessonViewHolder(private val binding: LessonItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun setLesson(lesson: Lesson) {
            binding.txtTitle.text = lesson.title
            binding.root.setOnClickListener {

                val fragmentManager = (context as FragmentActivity).supportFragmentManager
                val bundle = Bundle().apply {
                    putString("lessonId","${lesson.id}")
                }
//                val fragment=LessonFragment().apply { arguments=bundle }
                val fragment= LessonSegmentsFragment().apply { arguments=bundle }
                fragmentManager.beginTransaction().replace(R.id.fragment_container_view,fragment).commit()
            }
        }
    }


    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LessonViewHolder {
        this.context = parent.context
        val binding = LessonItemBinding.inflate(LayoutInflater.from(this.context), parent, false)
        return LessonViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return lessons.size
    }

    override fun onBindViewHolder(holder: LessonViewHolder, position: Int) {
        holder.setLesson(lessons[position])

    }
}