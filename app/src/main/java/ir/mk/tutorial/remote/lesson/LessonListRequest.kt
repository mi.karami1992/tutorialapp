package ir.mk.tutorial.remote.lesson


interface LessonListRequest {
    fun onSuccess(data:List<LessonRequestDto>)
    fun onNotSuccess(message:String)
    fun onError(message: String)
}