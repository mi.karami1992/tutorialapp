package ir.mk.tutorial.mvp.presenter

import ir.mk.tutorial.dto.LessonResponse
import ir.mk.tutorial.mvp.OnLessonBindData
import ir.mk.tutorial.mvp.model.ModelLesson
import ir.mk.tutorial.mvp.view.ViewLesson

class PresenterLesson(
    private val view: ViewLesson,
    private val model: ModelLesson
) {
    fun onCreate(lessonId: Int) {
        model.fetchLesson(object : OnLessonBindData {
            override fun loadLesson(lesson: LessonResponse) {
                view.showSegments(lesson.segments)
                view.showTitle(lesson.title)
            }
        }, lessonId)
    }

}