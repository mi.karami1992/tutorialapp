package ir.mk.tutorial.dto

data class LessonResponse(
    val id: Int,
    val title: String,
    val segments: List<Segment>
)
