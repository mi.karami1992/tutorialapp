package ir.mk.tutorial.mvp.presenter

import ir.mk.tutorial.mvp.view.ViewMainActivity

class PresenterMainActivity(
    private val view:ViewMainActivity,
) {
    fun onCreate(darkMode:Boolean,language:String){
        view.setNavigation(darkMode,language)
//        view.setBackButton()
        view.setDarkMode(darkMode)
        view.setLanguage(language)

    }


}