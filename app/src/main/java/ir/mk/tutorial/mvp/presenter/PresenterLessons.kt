package ir.mk.tutorial.mvp.presenter

import ir.mk.tutorial.dto.Lesson
import ir.mk.tutorial.mvp.OnLessonBindData
import ir.mk.tutorial.mvp.model.ModelLessons
import ir.mk.tutorial.mvp.view.ViewLessonsFragment

class PresenterLessons(
    private val view:ViewLessonsFragment,
    private val model:ModelLessons
) {
    fun onCreate(){
        model.fetchAllLessons(object :OnLessonBindData{
            override fun loadLessons(lessons: ArrayList<Lesson>) {
                view.showAdapter(lessons)
            }
        })
    }

}