package ir.mk.tutorial.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import ir.mk.tutorial.activity.ShowPhotoActivity
import ir.mk.tutorial.databinding.SegmentItemBinding
import ir.mk.tutorial.dto.Segment

class SegmentAdapter(
    private val segments: List<Segment>
) : RecyclerView.Adapter<SegmentAdapter.SegmentViewHolder>() {
    inner class SegmentViewHolder(private val binding: SegmentItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun setSegment(segment: Segment) {
            binding.segmentParagraph.text = segment.paragraph
            Glide.with(context).load(segment.imageUrl)
                .fitCenter()
                .into(binding.segmentImage)

            binding.segmentImage.setOnClickListener{
                val intent = Intent(context, ShowPhotoActivity::class.java)
                intent.putExtra("imageUrl", segment.imageUrl)
                context.startActivity(intent)
            }

        }
    }


    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SegmentViewHolder {
        this.context = parent.context
        val binding = SegmentItemBinding.inflate(LayoutInflater.from(this.context), parent, false)
        return SegmentViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return segments.size
    }

    override fun onBindViewHolder(holder: SegmentViewHolder, position: Int) {
        holder.setSegment(segments[position])

    }
}