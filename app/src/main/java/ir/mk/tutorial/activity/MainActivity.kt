package ir.mk.tutorial.activity

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ir.mk.tutorial.R
import ir.mk.tutorial.fragments.LessonsFragment
import ir.mk.tutorial.mvp.presenter.PresenterMainActivity
import ir.mk.tutorial.mvp.view.ViewMainActivity


class MainActivity : AppCompatActivity() {
    private lateinit var sharedPreferences: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        sharedPreferences = getSharedPreferences("pref", Context.MODE_PRIVATE)
        val darkMode = sharedPreferences.getBoolean("nightMode", false)
        val language = sharedPreferences.getString("language", "en").toString()
        val view = ViewMainActivity(this)
        val presenter = PresenterMainActivity(view)
        presenter.onCreate(darkMode, language)
        setContentView(view.binding.root)
    }


    override fun onBackPressed() {
        val fragment = supportFragmentManager.findFragmentById(R.id.fragment_container_view)
        if (fragment is LessonsFragment) {
            finish()
        } else {
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container_view, LessonsFragment()).commit()
        }
    }


}