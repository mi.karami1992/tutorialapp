package ir.mk.tutorial.mvp.view

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.WindowManager
import android.widget.FrameLayout
import com.bumptech.glide.Glide
import ir.mk.tutorial.activity.ShowPhotoActivity
import ir.mk.tutorial.databinding.ActivityShowPhotoBinding

class ViewShowPhotoActivity(contextInstance: Context) : FrameLayout(contextInstance) {
    val binding = ActivityShowPhotoBinding.inflate(LayoutInflater.from(context))
    private val activity = (context as? ShowPhotoActivity)!!

    fun loadImage(){
        val imageUrl = activity.intent.getStringExtra("imageUrl");
        Glide.with(this).load(imageUrl)
            .fitCenter()
            .into(binding.imageView)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            activity.window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            val attrib = activity.window.attributes
            attrib.layoutInDisplayCutoutMode =
                WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES

        } else {
            activity.window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
            )
        }
    }

    fun  initListener(){
        binding.imgClose.setOnClickListener {
            activity.finish()
        }
    }

}
