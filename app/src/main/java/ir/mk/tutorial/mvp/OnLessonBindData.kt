package ir.mk.tutorial.mvp

import ir.mk.tutorial.dto.Lesson
import ir.mk.tutorial.dto.LessonResponse

interface OnLessonBindData {
    fun loadLessons(lessons: ArrayList<Lesson>) {}
    fun loadLesson(lessons: LessonResponse) {}

}